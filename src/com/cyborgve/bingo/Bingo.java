package com.cyborgve.bingo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.UIManager;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;

import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.theme.SkyBluer;


public class Bingo extends JFrame implements ActionListener, WindowListener {

	private static final long serialVersionUID = 3283140963055936010L;
	private JPanel jcontentPane;
	private JPanel panelNorte;
	private JPanel panelCentro;
	private JPanel panelSur;
	private JPanel panelCantados;
	private JLabel lblB;
	private JLabel lblI;
	private JLabel lblN;
	private JLabel lblG;
	private JLabel lblO;
	private JButton cantar;
	private JButton reiniciar;
	private JPanel P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12,P13,P14,P15,
		  P16,P17,P18,P19,P20,P21,P22,P23,P24,P25,P26,P27,P28,P29,P30,
		  P31,P32,P33,P34,P35,P36,P37,P38,P39,P40,P41,P42,P43,P44,P45,
		  P46,P47,P48,P49,P50,P51,P52,P53,P54,P55,P56,P57,P58,P59,P60,
		  P61,P62,P63,P64,P65,P66,P67,P68,P69,P70,P71,P72,P73,P74,P75;
	private JLabel C1,C2,C3,C4,C5;
	private List<Integer> numeros;
	int posicionCantado;
	private JMenuBar menuBar;
	private JMenu mnArchivo;
	private JMenuItem mntmNuevoJuego;
	private JMenu mnTipoDeJuego;
	private JCheckBoxMenuItem chckbxmntmTotalmenteAleatorio;
	private JCheckBoxMenuItem chckbxmntmAleatorioPorLetra;
	private JMenuItem mntmAcercaDe;
	private JMenuItem mntmSalir;
	int tipoBingo;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				try {
					PlasticLookAndFeel.setPlasticTheme(new SkyBluer());
					PlasticLookAndFeel.setHighContrastFocusColorsEnabled(true);
					UIManager.setLookAndFeel("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException| UnsupportedLookAndFeelException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),"Error al Cargar Look&Feel",JOptionPane.ERROR_MESSAGE);
				}
				Bingo thisclass = new Bingo();
				thisclass.setLocationRelativeTo(null);
				thisclass.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				thisclass.setVisible(true);
				thisclass.setTitle("El Bingo del Co�o de su Madre");
			}
		});
	}
	
	public Bingo(){
		initialize();
	}
	
	private void initialize(){
		setJMenuBar(getMenuBar_1());
		this.setContentPane(getJContentPane());
		this.setSize(400, 508);
		this.setResizable(false);
		this.numeros = new ArrayList<>();
		this.posicionCantado = 1;
		this.tipoBingo = 0;
		this.getChckbxmntmTotalmenteAleatorio().setSelected(true);
		this.addWindowListener(this);
	}
	
	private JPanel getJContentPane(){
		if(jcontentPane == null){
			jcontentPane = new JPanel();
			jcontentPane.setLayout(new BorderLayout());
			jcontentPane.add(getPanelNorte(), BorderLayout.NORTH);
			jcontentPane.add(getPanelCentro(), BorderLayout.CENTER);
			jcontentPane.add(getPanelSur(), BorderLayout.SOUTH);
		}
		return jcontentPane;
	}
	
	private JPanel getPanelNorte(){
		if(panelNorte == null){
			panelNorte = new JPanel();
			panelNorte.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED), new CompoundBorder(new BevelBorder(BevelBorder.LOWERED), new BevelBorder(BevelBorder.RAISED))));
			GridLayout layout = new GridLayout();
			layout.setColumns(5);
			layout.setRows(1);
			panelNorte.setLayout(layout);
			panelNorte.add(getLblB());
			panelNorte.add(getLblI());
			panelNorte.add(getLblN());
			panelNorte.add(getLblG());
			panelNorte.add(getLblO());
		}
		return panelNorte;
	}
	
	private JPanel getPanelCentro(){
		if(panelCentro == null){
			panelCentro = new JPanel();
			panelCentro.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED), new CompoundBorder(new BevelBorder(BevelBorder.LOWERED), new BevelBorder(BevelBorder.RAISED))));
			GridLayout layout = new GridLayout();
			layout.setColumns(5);
			layout.setRows(15);
			panelCentro.setLayout(layout);
			panelCentro.add(getP1());
			panelCentro.add(getP16());
			panelCentro.add(getP31());
			panelCentro.add(getP46());
			panelCentro.add(getP61());
			panelCentro.add(getP2());
			panelCentro.add(getP17());
			panelCentro.add(getP32());
			panelCentro.add(getP47());
			panelCentro.add(getP62());
			panelCentro.add(getP3());
			panelCentro.add(getP18());
			panelCentro.add(getP33());
			panelCentro.add(getP48());
			panelCentro.add(getP63());
			panelCentro.add(getP4());
			panelCentro.add(getP19());
			panelCentro.add(getP34());
			panelCentro.add(getP49());
			panelCentro.add(getP64());
			panelCentro.add(getP5());
			panelCentro.add(getP20());
			panelCentro.add(getP35());
			panelCentro.add(getP50());
			panelCentro.add(getP65());
			panelCentro.add(getP6());
			panelCentro.add(getP21());
			panelCentro.add(getP36());
			panelCentro.add(getP51());
			panelCentro.add(getP66());
			panelCentro.add(getP7());
			panelCentro.add(getP22());
			panelCentro.add(getP37());
			panelCentro.add(getP52());
			panelCentro.add(getP67());
			panelCentro.add(getP8());
			panelCentro.add(getP23());
			panelCentro.add(getP38());
			panelCentro.add(getP53());
			panelCentro.add(getP68());
			panelCentro.add(getP9());
			panelCentro.add(getP24());
			panelCentro.add(getP39());
			panelCentro.add(getP54());
			panelCentro.add(getP69());
			panelCentro.add(getP10());
			panelCentro.add(getP25());
			panelCentro.add(getP40());
			panelCentro.add(getP55());
			panelCentro.add(getP70());
			panelCentro.add(getP11());
			panelCentro.add(getP26());
			panelCentro.add(getP41());
			panelCentro.add(getP56());
			panelCentro.add(getP71());
			panelCentro.add(getP12());
			panelCentro.add(getP27());
			panelCentro.add(getP42());
			panelCentro.add(getP57());
			panelCentro.add(getP72());
			panelCentro.add(getP13());
			panelCentro.add(getP28());
			panelCentro.add(getP43());
			panelCentro.add(getP58());
			panelCentro.add(getP73());
			panelCentro.add(getP14());
			panelCentro.add(getP29());
			panelCentro.add(getP44());
			panelCentro.add(getP59());
			panelCentro.add(getP74());
			panelCentro.add(getP15());
			panelCentro.add(getP30());
			panelCentro.add(getP45());
			panelCentro.add(getP60());
			panelCentro.add(getP75());	}
		return panelCentro;
	}
	
	private JPanel getPanelSur(){
		if(panelSur == null){
			panelSur = new JPanel();
			panelSur.setBorder(new BevelBorder(BevelBorder.RAISED));
			panelSur.setLayout(new BorderLayout());
			JPanel panelBotnones = new JPanel();
			panelBotnones.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED), new BevelBorder(BevelBorder.RAISED)));
			panelBotnones.setLayout(new GridLayout(1, 2, 0, 0));
			panelBotnones.add(getCantar());
			panelBotnones.add(getReiniciar());
			panelSur.add(panelBotnones, BorderLayout.EAST);
			panelSur.add(getPanelCantados(),BorderLayout.CENTER);
		}
		return panelSur;
	}
	
	private JLabel getC1(){
		if(C1 == null){
			C1 = new JLabel();
			C1.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 18));
			C1.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return C1;
	}
	
	private JLabel getC2(){
		if(C2 == null){
			C2 = new JLabel();
			C2.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 18));
			C2.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return C2;
	}
	
	private JLabel getC3(){
		if(C3 == null){
			C3 = new JLabel();
			C3.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 18));
			C3.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return C3;
	}
	
	private JLabel getC4(){
		if(C4 == null){
			C4 = new JLabel();
			C4.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 18));
			C4.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return C4;
	}
	
	private JLabel getC5(){
		if(C5 == null){
			C5 = new JLabel();
			C5.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 18));
			C5.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return C5;
	}
	
	private JPanel getPanelCantados(){
		if(panelCantados == null){
			panelCantados = new JPanel(new GridLayout(1,3));
			panelCantados.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED), new BevelBorder(BevelBorder.RAISED)));
			panelCantados.add(getPanel());
			panelCantados.add(getPanel_1());
			panelCantados.add(getPanel_2());
			panelCantados.add(getPanel_3());
			panelCantados.add(getPanel_4());
		}
		return panelCantados;
	}

	private JLabel getLblB() {
		if (lblB == null) {
			lblB = new JLabel("B");
			lblB.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblB.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblB;
	}
	private JLabel getLblI() {
		if (lblI == null) {
			lblI = new JLabel("I");
			lblI.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblI.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblI;
	}
	private JLabel getLblN() {
		if (lblN == null) {
			lblN = new JLabel("N");
			lblN.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblN.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblN;
	}
	private JLabel getLblG() {
		if (lblG == null) {
			lblG = new JLabel("G");
			lblG.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblG.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblG;
	}
	private JLabel getLblO() {
		if (lblO == null) {
			lblO = new JLabel("O");
			lblO.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblO.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblO;
	}
	
	private JButton getCantar(){
		if(cantar == null){
			cantar = new JButton("Cantar");
			cantar.addActionListener(this);
		}
		return cantar;
	}
	
	private JButton getReiniciar(){
		if(reiniciar == null){
			reiniciar = new JButton("Reiniciar");
			reiniciar.addActionListener(this);
		}
		return reiniciar;
	}
	
	private JPanel getP1(){
		if(P1 == null){
			P1 = new JPanel();
			P1.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P1.setBorder(new BevelBorder(BevelBorder.RAISED));
			P1.add(new JLabel("1"));
		}
		return P1;
	}
	
	private JPanel getP2(){
		if(P2 == null){
			P2 = new JPanel();
			P2.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P2.setBorder(new BevelBorder(BevelBorder.RAISED));
			P2.add(new JLabel("2"));
		}
		return P2;
	}
	
	private JPanel getP3(){
		if(P3 == null){
			P3 = new JPanel();
			P3.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P3.setBorder(new BevelBorder(BevelBorder.RAISED));
			P3.add(new JLabel("3"));
		}
		return P3;
	}
	
	private JPanel getP4(){
		if(P4 == null){
			P4 = new JPanel();
			P4.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P4.setBorder(new BevelBorder(BevelBorder.RAISED));
			P4.add(new JLabel("4"));
		}
		return P4;
	}
	
	private JPanel getP5(){
		if(P5 == null){
			P5 = new JPanel();
			P5.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P5.setBorder(new BevelBorder(BevelBorder.RAISED));
			P5.add(new JLabel("5"));
		}
		return P5;
	}
	
	private JPanel getP6(){
		if(P6 == null){
			P6 = new JPanel();
			P6.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P6.setBorder(new BevelBorder(BevelBorder.RAISED));
			P6.add(new JLabel("6"));
		}
		return P6;
	}
	
	private JPanel getP7(){
		if(P7 == null){
			P7 = new JPanel();
			P7.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P7.setBorder(new BevelBorder(BevelBorder.RAISED));
			P7.add(new JLabel("7"));
		}
		return P7;
	}
	
	private JPanel getP8(){
		if(P8 == null){
			P8 = new JPanel();
			P8.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P8.setBorder(new BevelBorder(BevelBorder.RAISED));
			P8.add(new JLabel("8"));
		}
		return P8;
	}
	
	private JPanel getP9(){
		if(P9 == null){
			P9 = new JPanel();
			P9.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P9.setBorder(new BevelBorder(BevelBorder.RAISED));
			P9.add(new JLabel("9"));
		}
		return P9;
	}
	
	private JPanel getP10(){
		if(P10 == null){
			P10 = new JPanel();
			P10.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P10.setBorder(new BevelBorder(BevelBorder.RAISED));
			P10.add(new JLabel("10"));
		}
		return P10;
	}
	
	private JPanel getP11(){
		if(P11 == null){
			P11 = new JPanel();
			P11.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P11.setBorder(new BevelBorder(BevelBorder.RAISED));
			P11.add(new JLabel("11"));
		}
		return P11;
	}
	
	private JPanel getP12(){
		if(P12 == null){
			P12 = new JPanel();
			P12.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P12.setBorder(new BevelBorder(BevelBorder.RAISED));
			P12.add(new JLabel("12"));
		}
		return P12;
	}
	
	private JPanel getP13(){
		if(P13 == null){
			P13 = new JPanel();
			P13.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P13.setBorder(new BevelBorder(BevelBorder.RAISED));
			P13.add(new JLabel("13"));
		}
		return P13;
	}
	
	private JPanel getP14(){
		if(P14 == null){
			P14 = new JPanel();
			P14.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P14.setBorder(new BevelBorder(BevelBorder.RAISED));
			P14.add(new JLabel("14"));
		}
		return P14;
	}
	
	private JPanel getP15(){
		if(P15 == null){
			P15 = new JPanel();
			P15.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P15.setBorder(new BevelBorder(BevelBorder.RAISED));
			P15.add(new JLabel("15"));
		}
		return P15;
	}
	
	private JPanel getP16(){
		if(P16 == null){
			P16 = new JPanel();
			P16.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P16.setBorder(new BevelBorder(BevelBorder.RAISED));
			P16.add(new JLabel("16"));
		}
		return P16;
	}
	
	private JPanel getP17(){
		if(P17 == null){
			P17 = new JPanel();
			P17.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P17.setBorder(new BevelBorder(BevelBorder.RAISED));
			P17.add(new JLabel("17"));
		}
		return P17;
	}
	
	private JPanel getP18(){
		if(P18 == null){
			P18 = new JPanel();
			P18.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P18.setBorder(new BevelBorder(BevelBorder.RAISED));
			P18.add(new JLabel("18"));
		}
		return P18;
	}
	
	private JPanel getP19(){
		if(P19 == null){
			P19 = new JPanel();
			P19.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P19.setBorder(new BevelBorder(BevelBorder.RAISED));
			P19.add(new JLabel("19"));
		}
		return P19;
	}
	
	private JPanel getP20(){
		if(P20 == null){
			P20 = new JPanel();
			P20.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P20.setBorder(new BevelBorder(BevelBorder.RAISED));
			P20.add(new JLabel("20"));
		}
		return P20;
	}
	
	private JPanel getP21(){
		if(P21 == null){
			P21 = new JPanel();
			P21.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P21.setBorder(new BevelBorder(BevelBorder.RAISED));
			P21.add(new JLabel("21"));
		}
		return P21;
	}
	
	private JPanel getP22(){
		if(P22 == null){
			P22 = new JPanel();
			P22.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P22.setBorder(new BevelBorder(BevelBorder.RAISED));
			P22.add(new JLabel("22"));
		}
		return P22;
	}
	
	private JPanel getP23(){
		if(P23 == null){
			P23 = new JPanel();
			P23.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P23.setBorder(new BevelBorder(BevelBorder.RAISED));
			P23.add(new JLabel("23"));
		}
		return P23;
	}
	
	private JPanel getP24(){
		if(P24 == null){
			P24 = new JPanel();
			P24.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P24.setBorder(new BevelBorder(BevelBorder.RAISED));
			P24.add(new JLabel("24"));
		}
		return P24;
	}
	
	private JPanel getP25(){
		if(P25 == null){
			P25 = new JPanel();
			P25.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P25.setBorder(new BevelBorder(BevelBorder.RAISED));
			P25.add(new JLabel("25"));
		}
		return P25;
	}
	
	private JPanel getP26(){
		if(P26 == null){
			P26 = new JPanel();
			P26.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P26.setBorder(new BevelBorder(BevelBorder.RAISED));
			P26.add(new JLabel("26"));
		}
		return P26;
	}
	
	private JPanel getP27(){
		if(P27 == null){
			P27 = new JPanel();
			P27.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P27.setBorder(new BevelBorder(BevelBorder.RAISED));
			P27.add(new JLabel("27"));
		}
		return P27;
	}
	
	private JPanel getP28(){
		if(P28 == null){
			P28 = new JPanel();
			P28.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P28.setBorder(new BevelBorder(BevelBorder.RAISED));
			P28.add(new JLabel("28"));
		}
		return P28;
	}
	
	private JPanel getP29(){
		if(P29 == null){
			P29 = new JPanel();
			P29.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P29.setBorder(new BevelBorder(BevelBorder.RAISED));
			P29.add(new JLabel("29"));
		}
		return P29;
	}
	
	private JPanel getP30(){
		if(P30 == null){
			P30 = new JPanel();
			P30.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P30.setBorder(new BevelBorder(BevelBorder.RAISED));
			P30.add(new JLabel("30"));
		}
		return P30;
	}
	
	private JPanel getP31(){
		if(P31 == null){
			P31 = new JPanel();
			P31.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P31.setBorder(new BevelBorder(BevelBorder.RAISED));
			P31.add(new JLabel("31"));
		}
		return P31;
	}
	
	private JPanel getP32(){
		if(P32 == null){
			P32 = new JPanel();
			P32.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P32.setBorder(new BevelBorder(BevelBorder.RAISED));
			P32.add(new JLabel("32"));
		}
		return P32;
	}
	
	private JPanel getP33(){
		if(P33 == null){
			P33 = new JPanel();
			P33.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P33.setBorder(new BevelBorder(BevelBorder.RAISED));
			P33.add(new JLabel("33"));
		}
		return P33;
	}
	
	private JPanel getP34(){
		if(P34 == null){
			P34 = new JPanel();
			P34.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P34.setBorder(new BevelBorder(BevelBorder.RAISED));
			P34.add(new JLabel("34"));
		}
		return P34;
	}
	
	private JPanel getP35(){
		if(P35 == null){
			P35 = new JPanel();
			P35.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P35.setBorder(new BevelBorder(BevelBorder.RAISED));
			P35.add(new JLabel("35"));
		}
		return P35;
	}
	
	private JPanel getP36(){
		if(P36 == null){
			P36 = new JPanel();
			P36.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P36.setBorder(new BevelBorder(BevelBorder.RAISED));
			P36.add(new JLabel("36"));
		}
		return P36;
	}
	
	private JPanel getP37(){
		if(P37 == null){
			P37 = new JPanel();
			P37.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P37.setBorder(new BevelBorder(BevelBorder.RAISED));
			P37.add(new JLabel("37"));
		}
		return P37;
	}
	
	private JPanel getP38(){
		if(P38 == null){
			P38 = new JPanel();
			P38.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P38.setBorder(new BevelBorder(BevelBorder.RAISED));
			P38.add(new JLabel("38"));
		}
		return P38;
	}
	
	private JPanel getP39(){
		if(P39 == null){
			P39 = new JPanel();
			P39.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P39.setBorder(new BevelBorder(BevelBorder.RAISED));
			P39.add(new JLabel("39"));
		}
		return P39;
	}
	
	private JPanel getP40(){
		if(P40 == null){
			P40 = new JPanel();
			P40.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P40.setBorder(new BevelBorder(BevelBorder.RAISED));
			P40.add(new JLabel("40"));
		}
		return P40;
	}
	
	private JPanel getP41(){
		if(P41 == null){
			P41 = new JPanel();
			P41.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P41.setBorder(new BevelBorder(BevelBorder.RAISED));
			P41.add(new JLabel("41"));
		}
		return P41;
	}
	
	private JPanel getP42(){
		if(P42 == null){
			P42 = new JPanel();
			P42.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P42.setBorder(new BevelBorder(BevelBorder.RAISED));
			P42.add(new JLabel("42"));
		}
		return P42;
	}
	
	private JPanel getP43(){
		if(P43 == null){
			P43 = new JPanel();
			P43.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P43.setBorder(new BevelBorder(BevelBorder.RAISED));
			P43.add(new JLabel("43"));
		}
		return P43;
	}
	
	private JPanel getP44(){
		if(P44 == null){
			P44 = new JPanel();
			P44.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P44.setBorder(new BevelBorder(BevelBorder.RAISED));
			P44.add(new JLabel("44"));
		}
		return P44;
	}
	
	private JPanel getP45(){
		if(P45 == null){
			P45 = new JPanel();
			P45.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P45.setBorder(new BevelBorder(BevelBorder.RAISED));
			P45.add(new JLabel("45"));
		}
		return P45;
	}
	
	private JPanel getP46(){
		if(P46 == null){
			P46 = new JPanel();
			P46.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P46.setBorder(new BevelBorder(BevelBorder.RAISED));
			P46.add(new JLabel("46"));
		}
		return P46;
	}
	
	private JPanel getP47(){
		if(P47 == null){
			P47 = new JPanel();
			P47.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P47.setBorder(new BevelBorder(BevelBorder.RAISED));
			P47.add(new JLabel("47"));
		}
		return P47;
	}
	
	private JPanel getP48(){
		if(P48 == null){
			P48 = new JPanel();
			P48.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P48.setBorder(new BevelBorder(BevelBorder.RAISED));
			P48.add(new JLabel("48"));
		}
		return P48;
	}
	
	private JPanel getP49(){
		if(P49 == null){
			P49 = new JPanel();
			P49.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P49.setBorder(new BevelBorder(BevelBorder.RAISED));
			P49.add(new JLabel("49"));
		}
		return P49;
	}
	
	private JPanel getP50(){
		if(P50 == null){
			P50 = new JPanel();
			P50.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P50.setBorder(new BevelBorder(BevelBorder.RAISED));
			P50.add(new JLabel("50"));
		}
		return P50;
	}
	
	private JPanel getP51(){
		if(P51 == null){
			P51 = new JPanel();
			P51.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P51.setBorder(new BevelBorder(BevelBorder.RAISED));
			P51.add(new JLabel("51"));
		}
		return P51;
	}
	
	private JPanel getP52(){
		if(P52 == null){
			P52 = new JPanel();
			P52.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P52.setBorder(new BevelBorder(BevelBorder.RAISED));
			P52.add(new JLabel("52"));
		}
		return P52;
	}
	
	private JPanel getP53(){
		if(P53 == null){
			P53 = new JPanel();
			P53.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P53.setBorder(new BevelBorder(BevelBorder.RAISED));
			P53.add(new JLabel("53"));
		}
		return P53;
	}
	
	private JPanel getP54(){
		if(P54 == null){
			P54 = new JPanel();
			P54.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P54.setBorder(new BevelBorder(BevelBorder.RAISED));
			P54.add(new JLabel("54"));
		}
		return P54;
	}
	
	private JPanel getP55(){
		if(P55 == null){
			P55 = new JPanel();
			P55.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P55.setBorder(new BevelBorder(BevelBorder.RAISED));
			P55.add(new JLabel("55"));
		}
		return P55;
	}
	
	private JPanel getP56(){
		if(P56 == null){
			P56 = new JPanel();
			P56.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P56.setBorder(new BevelBorder(BevelBorder.RAISED));
			P56.add(new JLabel("56"));
		}
		return P56;
	}
	
	private JPanel getP57(){
		if(P57 == null){
			P57 = new JPanel();
			P57.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P57.setBorder(new BevelBorder(BevelBorder.RAISED));
			P57.add(new JLabel("57"));
		}
		return P57;
	}
	
	private JPanel getP58(){
		if(P58 == null){
			P58 = new JPanel();
			P58.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P58.setBorder(new BevelBorder(BevelBorder.RAISED));
			P58.add(new JLabel("58"));
		}
		return P58;
	}
	
	private JPanel getP59(){
		if(P59 == null){
			P59 = new JPanel();
			P59.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P59.setBorder(new BevelBorder(BevelBorder.RAISED));
			P59.add(new JLabel("59"));
		}
		return P59;
	}
	
	private JPanel getP60(){
		if(P60 == null){
			P60 = new JPanel();
			P60.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P60.setBorder(new BevelBorder(BevelBorder.RAISED));
			P60.add(new JLabel("60"));
		}
		return P60;
	}
	
	private JPanel getP61(){
		if(P61 == null){
			P61 = new JPanel();
			P61.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P61.setBorder(new BevelBorder(BevelBorder.RAISED));
			P61.add(new JLabel("61"));
		}
		return P61;
	}
	
	private JPanel getP62(){
		if(P62 == null){
			P62 = new JPanel();
			P62.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P62.setBorder(new BevelBorder(BevelBorder.RAISED));
			P62.add(new JLabel("62"));
		}
		return P62;
	}
	
	private JPanel getP63(){
		if(P63 == null){
			P63 = new JPanel();
			P63.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P63.setBorder(new BevelBorder(BevelBorder.RAISED));
			P63.add(new JLabel("63"));
		}
		return P63;
	}
	
	private JPanel getP64(){
		if(P64 == null){
			P64 = new JPanel();
			P64.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P64.setBorder(new BevelBorder(BevelBorder.RAISED));
			P64.add(new JLabel("64"));
		}
		return P64;
	}
	
	private JPanel getP65(){
		if(P65 == null){
			P65 = new JPanel();
			P65.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P65.setBorder(new BevelBorder(BevelBorder.RAISED));
			P65.add(new JLabel("65"));
		}
		return P65;
	}
	
	private JPanel getP66(){
		if(P66 == null){
			P66 = new JPanel();
			P66.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P66.setBorder(new BevelBorder(BevelBorder.RAISED));
			P66.add(new JLabel("66"));
		}
		return P66;
	}
	
	private JPanel getP67(){
		if(P67 == null){
			P67 = new JPanel();
			P67.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P67.setBorder(new BevelBorder(BevelBorder.RAISED));
			P67.add(new JLabel("67"));
		}
		return P67;
	}
	
	private JPanel getP68(){
		if(P68 == null){
			P68 = new JPanel();
			P68.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P68.setBorder(new BevelBorder(BevelBorder.RAISED));
			P68.add(new JLabel("68"));
		}
		return P68;
	}
	
	private JPanel getP69(){
		if(P69 == null){
			P69 = new JPanel();
			P69.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P69.setBorder(new BevelBorder(BevelBorder.RAISED));
			P69.add(new JLabel("69"));
		}
		return P69;
	}
	
	private JPanel getP70(){
		if(P70 == null){
			P70 = new JPanel();
			P70.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P70.setBorder(new BevelBorder(BevelBorder.RAISED));
			P70.add(new JLabel("70"));
		}
		return P70;
	}
	
	private JPanel getP71(){
		if(P71 == null){
			P71 = new JPanel();
			P71.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P71.setBorder(new BevelBorder(BevelBorder.RAISED));
			P71.add(new JLabel("71"));
		}
		return P71;
	}
	
	private JPanel getP72(){
		if(P72 == null){
			P72 = new JPanel();
			P72.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P72.setBorder(new BevelBorder(BevelBorder.RAISED));
			P72.add(new JLabel("72"));
		}
		return P72;
	}
	
	private JPanel getP73(){
		if(P73 == null){
			P73 = new JPanel();
			P73.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P73.setBorder(new BevelBorder(BevelBorder.RAISED));
			P73.add(new JLabel("73"));
		}
		return P73;
	}
	
	private JPanel getP74(){
		if(P74 == null){
			P74 = new JPanel();
			P74.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P74.setBorder(new BevelBorder(BevelBorder.RAISED));
			P74.add(new JLabel("74"));
		}
		return P74;
	}
	
	private JPanel getP75(){
		if(P75 == null){
			P75 = new JPanel();
			P75.setFont(new Font("Lucida Grande", Font.BOLD, 14));
			P75.setBorder(new BevelBorder(BevelBorder.RAISED));
			P75.add(new JLabel("75"));
		}
		return P75;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(getCantar())){
			if(numeros.size() < 75){
				int numero = (tipoBingo == 0)
						?getNumeroAleatorio():getNumeroAleatorioOrdenado();
				if(tipoBingo == 1) posicionCantado++;
				if(posicionCantado == 6) posicionCantado = 1;
				numeros.add(numero);
				marcarCantado(numero);
				pintar(numero, UIManager.getColor("MenuItem.selectionBackground"));
			}else{
				JOptionPane.showMessageDialog(null, "El Limite es 75 Fichas\nYa todas han sido cantadas","El co�o de su madre",JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
		if((e.getSource().equals(getReiniciar()))||(e.getSource().equals(getMntmNuevoJuego()))) reiniciar();
		
		if(e.getSource().equals(getChckbxmntmTotalmenteAleatorio())){
			if(numeros.size() > 0){
				JOptionPane.showMessageDialog(null, "No se puede cambiar el tipo de juego\nen una partida iniciada","El Co�o de su Madre", JOptionPane.ERROR_MESSAGE);
				getChckbxmntmTotalmenteAleatorio().setSelected(false);
			}
			else{
				getChckbxmntmTotalmenteAleatorio().setSelected(true);
				getChckbxmntmAleatorioPorLetra().setSelected(false);
				tipoBingo = 0;
			}
		}
		
		if(e.getSource().equals(getChckbxmntmAleatorioPorLetra())){
			if(numeros.size() > 0){
				JOptionPane.showMessageDialog(null, "No se puede cambiar el tipo de juego\nen una partida iniciada","El Co�o de su Madre", JOptionPane.ERROR_MESSAGE);
				getChckbxmntmAleatorioPorLetra().setSelected(false);
			}
			else{
				getChckbxmntmTotalmenteAleatorio().setSelected(false);
				getChckbxmntmAleatorioPorLetra().setSelected(true);
				tipoBingo = 1;
			}
		}
		
		if(e.getSource().equals(getMntmAcercaDe())) new AcercaDe();
		
		if(e.getSource().equals(getMntmSalir())) salir();
	}
	
	private void salir(){
		int salir = JOptionPane.showConfirmDialog(null, "�Realmente deseas salir del Programa?","El co�o de su madre",JOptionPane.YES_NO_OPTION);
		if(salir == JOptionPane.YES_OPTION) System.exit(0);
	}
	
	private int getNumeroAleatorio(){
		int numero = ((int)(Math.random()*(75-1+1)+1));
		for(int i:numeros){
			if(i == numero){
				numero = getNumeroAleatorio();
				break;
			}
		}
		return numero;
	}
	
	private int getNumeroAleatorioOrdenado(){
		int numero = 0;
		switch (posicionCantado) {
		case 1:
			numero = ((int)(Math.random()*(15-1+1)+1));
			break;
		case 2:
			numero = ((int)(Math.random()*(30-16+1)+16));
			break;
		case 3:
			numero = ((int)(Math.random()*(45-31+1)+31));
			break;
		case 4:
			numero = ((int)(Math.random()*(60-46+1)+46));
			break;
		case 5:
			numero = ((int)(Math.random()*(75-61+1)+61));
			break;
		}
		for(int i: numeros){
			if(i == numero){
				numero = getNumeroAleatorioOrdenado();
				break;
			}				
		}
		return numero;
	}
	
	private void marcarCantado(int cantado){
		String canto = null;
		if(cantado <= 15) canto = "B-"+cantado;
		if(cantado >= 16 && cantado <= 30) canto = "I-"+cantado;
		if(cantado >= 31 && cantado <= 45) canto = "N-"+cantado;
		if(cantado >= 46 && cantado <= 60) canto = "G-"+cantado;
		if(cantado >= 61 && cantado <= 75) canto = "O-"+cantado;
		getC5().setText(getC4().getText());
		getC4().setText(getC3().getText());
		getC3().setText(getC2().getText());
		getC2().setText(getC1().getText());
		getC1().setText(canto);		
	}
	
	private void reiniciar(){
		int option = JOptionPane.showConfirmDialog(null, "Esta seguro de reiniciar la partida", "El co�o de su madre", JOptionPane.YES_NO_OPTION);
		if(option == JOptionPane.YES_OPTION){
			for(int i = 0;i < 75; i++)
				pintar(i+1, UIManager.getColor("Panel.background"));
			numeros = new ArrayList<>();
			getC1().setText("");
			getC2().setText("");
			getC3().setText("");
			getC4().setText("");
			getC5().setText("");
			posicionCantado = 1;
		}
	}
	
	private void pintar(int i, Color color){
		switch (i) {
		case 1:
			getP1().setBackground(color);
			break;
		case 2:
			getP2().setBackground(color);
			break;
		case 3:
			getP3().setBackground(color);
			break;
		case 4:
			getP4().setBackground(color);
			break;
		case 5:
			getP5().setBackground(color);
			break;
		case 6:
			getP6().setBackground(color);
			break;
		case 7:
			getP7().setBackground(color);
			break;
		case 8:
			getP8().setBackground(color);
			break;
		case 9:
			getP9().setBackground(color);
			break;
		case 10:
			getP10().setBackground(color);
			break;
		case 11:
			getP11().setBackground(color);
			break;
		case 12:
			getP12().setBackground(color);
			break;
		case 13:
			getP13().setBackground(color);
			break;
		case 14:
			getP14().setBackground(color);
			break;
		case 15:
			getP15().setBackground(color);
			break;
		case 16:
			getP16().setBackground(color);
			break;
		case 17:
			getP17().setBackground(color);
			break;
		case 18:
			getP18().setBackground(color);
			break;
		case 19:
			getP19().setBackground(color);
			break;
		case 20:
			getP20().setBackground(color);
			break;
		case 21:
			getP21().setBackground(color);
			break;
		case 22:
			getP22().setBackground(color);
			break;
		case 23:
			getP23().setBackground(color);
			break;
		case 24:
			getP24().setBackground(color);
			break;
		case 25:
			getP25().setBackground(color);
			break;
		case 26:
			getP26().setBackground(color);
			break;
		case 27:
			getP27().setBackground(color);
			break;
		case 28:
			getP28().setBackground(color);
			break;
		case 29:
			getP29().setBackground(color);
			break;
		case 30:
			getP30().setBackground(color);
			break;
		case 31:
			getP31().setBackground(color);
			break;
		case 32:
			getP32().setBackground(color);
			break;
		case 33:
			getP33().setBackground(color);
			break;
		case 34:
			getP34().setBackground(color);
			break;
		case 35:
			getP35().setBackground(color);
			break;
		case 36:
			getP36().setBackground(color);
			break;
		case 37:
			getP37().setBackground(color);
			break;
		case 38:
			getP38().setBackground(color);
			break;
		case 39:
			getP39().setBackground(color);
			break;
		case 40:
			getP40().setBackground(color);
			break;
		case 41:
			getP41().setBackground(color);
			break;
		case 42:
			getP42().setBackground(color);
			break;
		case 43:
			getP43().setBackground(color);
			break;
		case 44:
			getP44().setBackground(color);
			break;
		case 45:
			getP45().setBackground(color);
			break;
		case 46:
			getP46().setBackground(color);
			break;
		case 47:
			getP47().setBackground(color);
			break;
		case 48:
			getP48().setBackground(color);
			break;
		case 49:
			getP49().setBackground(color);
			break;
		case 50:
			getP50().setBackground(color);
			break;
		case 51:
			getP51().setBackground(color);
			break;
		case 52:
			getP52().setBackground(color);
			break;
		case 53:
			getP53().setBackground(color);
			break;
		case 54:
			getP54().setBackground(color);
			break;
		case 55:
			getP55().setBackground(color);
			break;
		case 56:
			getP56().setBackground(color);
			break;
		case 57:
			getP57().setBackground(color);
			break;
		case 58:
			getP58().setBackground(color);
			break;
		case 59:
			getP59().setBackground(color);
			break;
		case 60:
			getP60().setBackground(color);
			break;
		case 61:
			getP61().setBackground(color);
			break;
		case 62:
			getP62().setBackground(color);
			break;
		case 63:
			getP63().setBackground(color);
			break;
		case 64:
			getP64().setBackground(color);
			break;
		case 65:
			getP65().setBackground(color);
			break;
		case 66:
			getP66().setBackground(color);
			break;
		case 67:
			getP67().setBackground(color);
			break;
		case 68:
			getP68().setBackground(color);
			break;
		case 69:
			getP69().setBackground(color);
			break;
		case 70:
			getP70().setBackground(color);
			break;
		case 71:
			getP71().setBackground(color);
			break;
		case 72:
			getP72().setBackground(color);
			break;
		case 73:
			getP73().setBackground(color);
			break;
		case 74:
			getP74().setBackground(color);
			break;
		case 75:
			getP75().setBackground(color);
			break;
		default:
			break;
		}
	}
	private JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnArchivo());
		}
		return menuBar;
	}
	private JMenu getMnArchivo() {
		if (mnArchivo == null) {
			mnArchivo = new JMenu("Archivo");
			mnArchivo.add(getMntmNuevoJuego());
			mnArchivo.add(getMnTipoDeJuego());
			mnArchivo.add(getMntmAcercaDe());
			mnArchivo.add(getMntmSalir());
		}
		return mnArchivo;
	}
	private JMenuItem getMntmNuevoJuego() {
		if (mntmNuevoJuego == null) {
			mntmNuevoJuego = new JMenuItem("Nuevo Juego");
			mntmNuevoJuego.addActionListener(this);
		}
		return mntmNuevoJuego;
	}
	private JMenu getMnTipoDeJuego() {
		if (mnTipoDeJuego == null) {
			mnTipoDeJuego = new JMenu("Tipo de Juego");
			mnTipoDeJuego.add(getChckbxmntmTotalmenteAleatorio());
			mnTipoDeJuego.add(getChckbxmntmAleatorioPorLetra());
		}
		return mnTipoDeJuego;
	}
	private JCheckBoxMenuItem getChckbxmntmTotalmenteAleatorio() {
		if (chckbxmntmTotalmenteAleatorio == null) {
			chckbxmntmTotalmenteAleatorio = new JCheckBoxMenuItem("Totalmente aleatorio");
			chckbxmntmTotalmenteAleatorio.addActionListener(this);
		}
		return chckbxmntmTotalmenteAleatorio;
	}
	private JCheckBoxMenuItem getChckbxmntmAleatorioPorLetra() {
		if (chckbxmntmAleatorioPorLetra == null) {
			chckbxmntmAleatorioPorLetra = new JCheckBoxMenuItem("Aleatorio por letra");
			chckbxmntmAleatorioPorLetra.addActionListener(this);
		}
		return chckbxmntmAleatorioPorLetra;
	}
	private JMenuItem getMntmAcercaDe() {
		if (mntmAcercaDe == null) {
			mntmAcercaDe = new JMenuItem("Acerca de");
			mntmAcercaDe.addActionListener(this);
		}
		return mntmAcercaDe;
	}
	private JMenuItem getMntmSalir() {
		if (mntmSalir == null) {
			mntmSalir = new JMenuItem("Salir");
			mntmSalir.addActionListener(this);
		}
		return mntmSalir;
	}

	@Override
	public void windowOpened(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		salir();		
	}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowDeactivated(WindowEvent e) {}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBackground(new Color(250, 250, 210));
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getC1());
		}
		return panel;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBackground(new Color(255, 228, 225));
			panel_1.setLayout(new BorderLayout(0, 0));
			panel_1.add(getC2());
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBackground(new Color(255, 255, 240));
			panel_2.setLayout(new BorderLayout(0, 0));
			panel_2.add(getC3());
		}
		return panel_2;
	}
	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.setBackground(new Color(230, 230, 250));
			panel_3.setLayout(new BorderLayout(0, 0));
			panel_3.add(getC4());
		}
		return panel_3;
	}
	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.setBackground(new Color(224, 255, 255));
			panel_4.setLayout(new BorderLayout(0, 0));
			panel_4.add(getC5());
		}
		return panel_4;
	}
}
