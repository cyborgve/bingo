package com.cyborgve.bingo;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import java.awt.Color;
import java.awt.BorderLayout;


public class AcercaDe extends JDialog implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2278669875873356139L;
	private PanelImagen contentPanel;
	private JTextArea txtrEsteProgramaEs;
	
	private PanelImagen getContentPanel(){
		if(contentPanel == null){
			contentPanel = new PanelImagen("/com/cyborgve/bingo/images/vieja.jpg");
			contentPanel.addMouseListener(this);
		}
		return contentPanel;
	}

	/**
	 * Create the dialog.
	 */
	public AcercaDe() {
		this.setSize(400, 508);
		this.setContentPane(getContentPanel());
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setAlwaysOnTop(true);
		this.setUndecorated(true);
		this.setVisible(true);
		this.addMouseListener(this);
		getContentPane().setLayout(new BorderLayout(0, 0));
		getContentPane().add(getTxtrEsteProgramaEs(), BorderLayout.SOUTH);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		this.dispose();
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
	
	class PanelImagen extends JPanel{
		private static final long serialVersionUID = 6723170841397179021L;
		ImageIcon icon;
		
		public PanelImagen(String string){
			this.icon = new ImageIcon(this.getClass().getResource(string));
		}

		@Override
		protected void paintComponent(Graphics g) {
			Dimension dimension = this.getSize();
			if(icon != null)
				g.drawImage(icon.getImage(),0,0,dimension.width,dimension.height,null);
			boolean opaque = (icon == null)?true:false;
			this.setOpaque(opaque);
			super.paintComponent(g);
		}
		
	}
	private JTextArea getTxtrEsteProgramaEs() {
		if (txtrEsteProgramaEs == null) {
			txtrEsteProgramaEs = new JTextArea();
			txtrEsteProgramaEs.setForeground(Color.WHITE);
			txtrEsteProgramaEs.setWrapStyleWord(true);
			txtrEsteProgramaEs.setText("         Este programa es una mierda que hice para unas primas\n putas que cuando no tienen clientes se ponen a jugar  bingo. \n                          Barquisimeto 9 de Mayo de 2014.      Richard.\n");
			txtrEsteProgramaEs.setOpaque(false);
		}
		return txtrEsteProgramaEs;
	}
}
